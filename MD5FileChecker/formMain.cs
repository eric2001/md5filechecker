// Project:  MD5FileChecker
// Start Date:  09 October 2007
// Last Update: 26 November 2007
// License:  GPL Version 3
// Description:  Compares all files in one folder to all files in a second
//				 folder, using MD5 checksums.
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace MD5FileChecker
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmCompareFiles : System.Windows.Forms.Form
	{
		DateTime startTime;

		private System.Windows.Forms.ListView lstFolder1;
		private System.Windows.Forms.ColumnHeader columnFileName;
		private System.Windows.Forms.ColumnHeader columnChecksum;
		private System.Windows.Forms.TextBox txtFolder1;
		private System.Windows.Forms.Button buttonSetFolder1;
		private System.Windows.Forms.Button buttonBrowse;
		private System.Windows.Forms.TextBox txtFolder2;
		private System.Windows.Forms.Button buttonBrowseFolder2;
		private System.Windows.Forms.GroupBox groupFolder1;
		private System.Windows.Forms.GroupBox groupFolder2;
		private System.Windows.Forms.ListView lstFolder2;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Label lblElapsedTime;
		private System.Windows.Forms.Timer timerElapsedTimeCounter;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuFile;
		private System.Windows.Forms.MenuItem menuExit;
		private System.Windows.Forms.MenuItem menuAbout;
		private System.ComponentModel.IContainer components;

		public frmCompareFiles()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.lstFolder1 = new System.Windows.Forms.ListView();
			this.columnFileName = new System.Windows.Forms.ColumnHeader();
			this.columnChecksum = new System.Windows.Forms.ColumnHeader();
			this.txtFolder1 = new System.Windows.Forms.TextBox();
			this.buttonSetFolder1 = new System.Windows.Forms.Button();
			this.buttonBrowse = new System.Windows.Forms.Button();
			this.txtFolder2 = new System.Windows.Forms.TextBox();
			this.buttonBrowseFolder2 = new System.Windows.Forms.Button();
			this.groupFolder1 = new System.Windows.Forms.GroupBox();
			this.groupFolder2 = new System.Windows.Forms.GroupBox();
			this.lstFolder2 = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.timerElapsedTimeCounter = new System.Windows.Forms.Timer(this.components);
			this.lblElapsedTime = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panel2 = new System.Windows.Forms.Panel();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuFile = new System.Windows.Forms.MenuItem();
			this.menuExit = new System.Windows.Forms.MenuItem();
			this.menuAbout = new System.Windows.Forms.MenuItem();
			this.groupFolder1.SuspendLayout();
			this.groupFolder2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// lstFolder1
			// 
			this.lstFolder1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lstFolder1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						 this.columnFileName,
																						 this.columnChecksum});
			this.lstFolder1.Location = new System.Drawing.Point(8, 48);
			this.lstFolder1.Name = "lstFolder1";
			this.lstFolder1.Size = new System.Drawing.Size(584, 120);
			this.lstFolder1.TabIndex = 0;
			this.lstFolder1.View = System.Windows.Forms.View.Details;
			// 
			// columnFileName
			// 
			this.columnFileName.Text = "File Name";
			this.columnFileName.Width = 143;
			// 
			// columnChecksum
			// 
			this.columnChecksum.Text = "Checksum";
			this.columnChecksum.Width = 306;
			// 
			// txtFolder1
			// 
			this.txtFolder1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtFolder1.Location = new System.Drawing.Point(8, 16);
			this.txtFolder1.Name = "txtFolder1";
			this.txtFolder1.Size = new System.Drawing.Size(496, 20);
			this.txtFolder1.TabIndex = 1;
			this.txtFolder1.Text = "";
			// 
			// buttonSetFolder1
			// 
			this.buttonSetFolder1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonSetFolder1.Location = new System.Drawing.Point(504, 8);
			this.buttonSetFolder1.Name = "buttonSetFolder1";
			this.buttonSetFolder1.Size = new System.Drawing.Size(80, 23);
			this.buttonSetFolder1.TabIndex = 2;
			this.buttonSetFolder1.Text = "Load Folders";
			this.buttonSetFolder1.Click += new System.EventHandler(this.buttonSetFolder1_Click);
			// 
			// buttonBrowse
			// 
			this.buttonBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonBrowse.Location = new System.Drawing.Point(512, 16);
			this.buttonBrowse.Name = "buttonBrowse";
			this.buttonBrowse.TabIndex = 3;
			this.buttonBrowse.Text = "Browse";
			this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
			// 
			// txtFolder2
			// 
			this.txtFolder2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtFolder2.Location = new System.Drawing.Point(8, 16);
			this.txtFolder2.Name = "txtFolder2";
			this.txtFolder2.Size = new System.Drawing.Size(496, 20);
			this.txtFolder2.TabIndex = 4;
			this.txtFolder2.Text = "";
			// 
			// buttonBrowseFolder2
			// 
			this.buttonBrowseFolder2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonBrowseFolder2.Location = new System.Drawing.Point(512, 16);
			this.buttonBrowseFolder2.Name = "buttonBrowseFolder2";
			this.buttonBrowseFolder2.TabIndex = 5;
			this.buttonBrowseFolder2.Text = "Browse";
			this.buttonBrowseFolder2.Click += new System.EventHandler(this.buttonBrowseFolder2_Click);
			// 
			// groupFolder1
			// 
			this.groupFolder1.Controls.Add(this.lstFolder1);
			this.groupFolder1.Controls.Add(this.buttonBrowse);
			this.groupFolder1.Controls.Add(this.txtFolder1);
			this.groupFolder1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupFolder1.Location = new System.Drawing.Point(0, 0);
			this.groupFolder1.Name = "groupFolder1";
			this.groupFolder1.Size = new System.Drawing.Size(600, 173);
			this.groupFolder1.TabIndex = 6;
			this.groupFolder1.TabStop = false;
			this.groupFolder1.Text = "Folder 1";
			// 
			// groupFolder2
			// 
			this.groupFolder2.Controls.Add(this.lstFolder2);
			this.groupFolder2.Controls.Add(this.buttonBrowseFolder2);
			this.groupFolder2.Controls.Add(this.txtFolder2);
			this.groupFolder2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.groupFolder2.Location = new System.Drawing.Point(0, 173);
			this.groupFolder2.Name = "groupFolder2";
			this.groupFolder2.Size = new System.Drawing.Size(600, 232);
			this.groupFolder2.TabIndex = 7;
			this.groupFolder2.TabStop = false;
			this.groupFolder2.Text = "Folder 2";
			// 
			// lstFolder2
			// 
			this.lstFolder2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lstFolder2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						 this.columnHeader1,
																						 this.columnHeader2});
			this.lstFolder2.Location = new System.Drawing.Point(8, 48);
			this.lstFolder2.Name = "lstFolder2";
			this.lstFolder2.Size = new System.Drawing.Size(584, 112);
			this.lstFolder2.TabIndex = 6;
			this.lstFolder2.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "File Name";
			this.columnHeader1.Width = 143;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Checksum";
			this.columnHeader2.Width = 307;
			// 
			// progressBar1
			// 
			this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.progressBar1.Location = new System.Drawing.Point(0, 36);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(608, 23);
			this.progressBar1.TabIndex = 8;
			// 
			// timerElapsedTimeCounter
			// 
			this.timerElapsedTimeCounter.Tick += new System.EventHandler(this.timerElapsedTimeCounter_Tick);
			// 
			// lblElapsedTime
			// 
			this.lblElapsedTime.AutoSize = true;
			this.lblElapsedTime.Location = new System.Drawing.Point(8, 8);
			this.lblElapsedTime.Name = "lblElapsedTime";
			this.lblElapsedTime.Size = new System.Drawing.Size(77, 16);
			this.lblElapsedTime.TabIndex = 9;
			this.lblElapsedTime.Text = "Elapsed Time:";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.splitter1);
			this.panel1.Controls.Add(this.groupFolder1);
			this.panel1.Controls.Add(this.groupFolder2);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(600, 405);
			this.panel1.TabIndex = 10;
			// 
			// splitter1
			// 
			this.splitter1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.splitter1.Location = new System.Drawing.Point(0, 170);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(600, 3);
			this.splitter1.TabIndex = 7;
			this.splitter1.TabStop = false;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.lblElapsedTime);
			this.panel2.Controls.Add(this.buttonSetFolder1);
			this.panel2.Controls.Add(this.progressBar1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel2.Location = new System.Drawing.Point(0, 341);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(600, 64);
			this.panel2.TabIndex = 11;
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuFile,
																					  this.menuAbout});
			// 
			// menuFile
			// 
			this.menuFile.Index = 0;
			this.menuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.menuExit});
			this.menuFile.Text = "File";
			// 
			// menuExit
			// 
			this.menuExit.Index = 0;
			this.menuExit.Text = "Exit";
			this.menuExit.Click += new System.EventHandler(this.menuExit_Click);
			// 
			// menuAbout
			// 
			this.menuAbout.Index = 1;
			this.menuAbout.Text = "About";
			this.menuAbout.Click += new System.EventHandler(this.menuAbout_Click);
			// 
			// frmCompareFiles
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(600, 405);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Menu = this.mainMenu1;
			this.Name = "frmCompareFiles";
			this.Text = "Compare Files";
			this.groupFolder1.ResumeLayout(false);
			this.groupFolder2.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]


		static void Main() 
		{
			Application.Run(new frmCompareFiles());
		}

		private void buttonSetFolder1_Click(object sender, System.EventArgs e)
		{
			md5 md5Generator = new md5();
			string diffFiles = "";
			System.IO.DirectoryInfo dir1;
			System.IO.DirectoryInfo dir2;

			// Start the Timer
			startTime = DateTime.Now;
			timerElapsedTimeCounter.Enabled = true;

			// Check to make sure two folders were selected, and that both
			//	folders are valid.
			if (txtFolder1.Text.Length == 0)
			{
				MessageBox.Show("Please specify a folder for Folder 1.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			if (!System.IO.Directory.Exists(txtFolder1.Text))
			{
				MessageBox.Show("Folder 1 Does Not Exist.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			if (txtFolder2.Text.Length == 0)
			{
				MessageBox.Show("Please specify a folder for Folder 2.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			if (!System.IO.Directory.Exists(txtFolder2.Text))
			{
				MessageBox.Show("Folder 2 Does Not Exist.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			
			// Reset the file lists.
			lstFolder1.Items.Clear();
			lstFolder2.Items.Clear();

			// Generate a list of files in each folder.
			getFilesInFolder(txtFolder1.Text, lstFolder1);
			getFilesInFolder(txtFolder2.Text, lstFolder2);

			// Make sure both folders have the same number of files in them.
			if (lstFolder1.Items.Count != lstFolder2.Items.Count)
			{
				MessageBox.Show("Folder 1 and Folder 2 do not have the same number of files.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			// Generate MD5 checksums for each file in Folder 1.
			progressBar1.Maximum = lstFolder1.Items.Count;
			int counter = 0; 
			progressBar1.Value = 0;
			foreach (ListViewItem oneFile in lstFolder1.Items) 
			{
				lstFolder1.Items[counter].SubItems.Add(md5Generator.generateMD5 (oneFile.Text));
				Application.DoEvents();
				counter++;
				progressBar1.Value = counter;
			}
			
			// Generate MD5 checksums for each file in Folder 2.
			progressBar1.Value = 0;
			progressBar1.Maximum = lstFolder2.Items.Count;
			counter = 0;
			foreach (ListViewItem oneFile in lstFolder2.Items) 
			{
				lstFolder2.Items[counter].SubItems.Add(md5Generator.generateMD5 (oneFile.Text));
				Application.DoEvents();
				counter++;
				progressBar1.Value = counter;
			}

			// Sort the file lists before comparing the files
			lstFolder1.Sorting = System.Windows.Forms.SortOrder.Ascending;
			lstFolder2.Sorting = System.Windows.Forms.SortOrder.Ascending;
			lstFolder1.Sort();
			lstFolder2.Sort();

			// Compare the MD5 Checksums for the files in both folders
			progressBar1.Value = 0;
			counter = 0;
			while (counter < lstFolder1.Items.Count)
			{
				// Check File Names
				if (lstFolder1.Items[counter].SubItems[0].Text.Substring(lstFolder1.Items[counter].SubItems[0].Text.LastIndexOf("\\")) != lstFolder2.Items[counter].SubItems[0].Text.Substring(lstFolder2.Items[counter].SubItems[0].Text.LastIndexOf("\\")))
					diffFiles+="File Names Did Not Match For " + lstFolder1.Items[counter].SubItems[0].Text.Substring(lstFolder1.Items[counter].SubItems[0].Text.LastIndexOf("\\")) + ", " + lstFolder2.Items[counter].SubItems[0].Text.Substring(lstFolder2.Items[counter].SubItems[0].Text.LastIndexOf("\\")) + "\n\n";

				// Check MD5 Value
				if (lstFolder1.Items[counter].SubItems[1].Text != lstFolder2.Items[counter].SubItems[1].Text)
					diffFiles+="MD5 Did Not Match For " + lstFolder1.Items[counter].SubItems[0].Text.Substring(lstFolder1.Items[counter].SubItems[0].Text.LastIndexOf("\\")) + " (" + lstFolder1.Items[counter].SubItems[1].Text + ", " + lstFolder2.Items[counter].SubItems[1].Text + ")\n";
				counter++;
				progressBar1.Value = counter;
			}

			// Unsort the lists
			lstFolder1.Sorting = System.Windows.Forms.SortOrder.None;
			lstFolder2.Sorting = System.Windows.Forms.SortOrder.None;

			// Disable the Timer
			timerElapsedTimeCounter.Enabled = false;

			// If anything didn't match, show a message
			if (diffFiles.Length > 0) 
			{
			    formErrors errorWindow = new formErrors();
				errorWindow.txtErrorFiles.Text = diffFiles;
				errorWindow.Show();
			}
			else
				MessageBox.Show("All Files Matched.", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void buttonBrowse_Click(object sender, System.EventArgs e)
		{
			// Set the path for txtFolder1 using a FolderBrowserDialog
			FolderBrowserDialog path1 = new FolderBrowserDialog();
			if (path1.ShowDialog() == DialogResult.OK)
				txtFolder1.Text = path1.SelectedPath;
		}

		private void buttonBrowseFolder2_Click(object sender, System.EventArgs e)
		{
			// Set the path for txtFolder2 using a FolderBrowserDialog
			FolderBrowserDialog path1 = new FolderBrowserDialog();
			if (path1.ShowDialog() == DialogResult.OK)
				txtFolder2.Text = path1.SelectedPath;
		}

		private void timerElapsedTimeCounter_Tick(object sender, System.EventArgs e)
		{
			// Update the Elapsed Time Label.

			// Declare Variables
			TimeSpan tempTime;
			string tempHours, tempMinutes, tempSeconds;

			// Figure out how much time has passed.
			tempTime = DateTime.Now - startTime;

			// Format the elapased time as a string.
			tempHours = tempTime.Hours.ToString();
			tempMinutes = tempTime.Minutes.ToString();
			tempSeconds = tempTime.Seconds.ToString();
			if (tempHours.Length == 1)
				tempHours = "0" + tempHours;
			if (tempMinutes.Length == 1)
				tempMinutes = "0" + tempMinutes;
			if (tempSeconds.Length == 1)
				tempSeconds = "0" + tempSeconds;

			// Update the Elapsed Time label.
			lblElapsedTime.Text = "Elapsed Time:  " + tempHours + ":" + tempMinutes + ":" + tempSeconds;
		}

		private void getFilesInFolder(string path, ListView listViewFiles)
		{
			// This function will generate a list of all files in the folder
			//     specified by "path" as well as files in any sub-folders and
			//	   store the list in the ListView specified by listViewFiles.
			System.IO.DirectoryInfo fileList;

			// If "path" is not a valid folder, then exit.
			if (!System.IO.Directory.Exists(path))
				return;

			// Loop through each file in "path", adding it to listViewFiles.
			fileList = new System.IO.DirectoryInfo(path);
			foreach (System.IO.FileInfo f in fileList.GetFiles()) 
			{
				listViewFiles.Items.Add(f.FullName);
				Application.DoEvents();
			}

			// Loop through each folder in "path", generating a list of any files
			//     within it.
			foreach (System.IO.DirectoryInfo d in fileList.GetDirectories())
			{
				getFilesInFolder(d.FullName, listViewFiles);
				Application.DoEvents();
			}
		}

		private void menuExit_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void menuAbout_Click(object sender, System.EventArgs e)
		{
			// Generate and display copyright text.
			string aboutText = "MD5 File Checker Version 1.3.0\n";
			aboutText += "Copyright 2008 Eric Cavaliere\n\n";
			aboutText += "This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\n";
			aboutText +="This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.\n\n";
			aboutText += "You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.";
			MessageBox.Show(aboutText, "About", MessageBoxButtons.OK);

		}
	}
}

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace MD5FileChecker
{
	/// <summary>
	/// Summary description for formErrors.
	/// </summary>
	public class formErrors : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label lblCheckFailed;
		private System.Windows.Forms.Button buttonOk;
		public System.Windows.Forms.RichTextBox txtErrorFiles;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public formErrors()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblCheckFailed = new System.Windows.Forms.Label();
			this.buttonOk = new System.Windows.Forms.Button();
			this.txtErrorFiles = new System.Windows.Forms.RichTextBox();
			this.SuspendLayout();
			// 
			// lblCheckFailed
			// 
			this.lblCheckFailed.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.lblCheckFailed.AutoSize = true;
			this.lblCheckFailed.Location = new System.Drawing.Point(212, 8);
			this.lblCheckFailed.Name = "lblCheckFailed";
			this.lblCheckFailed.Size = new System.Drawing.Size(265, 16);
			this.lblCheckFailed.TabIndex = 0;
			this.lblCheckFailed.Text = "The MD5 File Check Failed For The Following Files:";
			// 
			// buttonOk
			// 
			this.buttonOk.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.buttonOk.Location = new System.Drawing.Point(307, 298);
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.TabIndex = 2;
			this.buttonOk.Text = "Ok";
			this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
			// 
			// txtErrorFiles
			// 
			this.txtErrorFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtErrorFiles.Location = new System.Drawing.Point(8, 24);
			this.txtErrorFiles.Name = "txtErrorFiles";
			this.txtErrorFiles.Size = new System.Drawing.Size(672, 264);
			this.txtErrorFiles.TabIndex = 3;
			this.txtErrorFiles.Text = "";
			// 
			// formErrors
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(688, 326);
			this.Controls.Add(this.txtErrorFiles);
			this.Controls.Add(this.buttonOk);
			this.Controls.Add(this.lblCheckFailed);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "formErrors";
			this.Text = "File Check Failed";
			this.ResumeLayout(false);

		}
		#endregion

		private void buttonOk_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}
	}
}

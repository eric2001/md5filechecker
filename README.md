# MD5FileChecker
![MD5FileChecker](./screenshot.jpg) 

## Description
This program will compare the contents of two directories, and alert you to any files that are not identical.  It does so by generating a list of files in each directory, generating MD5 checksums for each file, and then comparing the checksums.  If the checksums match, then it is assumed the files are the same.    This program was primarily created to confirm that files copied from one hard drive to another are transfered without errors.  More information on MD5 can be found on [http://en.wikipedia.org/wiki/Md5 Wikipedia].

**License:** [GPL v.3](http://www.gnu.org/licenses/gpl-3.0.html)

## Instructions
Specify two folders to compare (Folder 1 and Folder 2).  Press "Load Folders".  The program will do the rest.  

Please be aware that this program was designed to compare two '''identical''' folders to each other to check for errors in copying.  If the folders have a different number of files in them or if the file names are different, it will not work properly.

## History
**Version 1.3.0:**
> - Added an About screen.
> - Added a scrollbar to the output screen for files that didn't match so as to prevent the box from going off the bottom of the screen.
> - Released 14 May 2008.
>
> Download: [Version 1.3.0 Setup](/uploads/3efa916cf5d88d717c6fb1817cd9dfba/MD5FileChecker1.3Setup.zip) | [Version 1.3.0 Source](/uploads/068ac107278246257bc5cb72ea140a88/MD5FileChecker1.3Source.zip)

**Version 1.2.0:**
> - Bug Fix:  Window now resizes properly.
> - Added support for sub-folders
> - Released on 26 November 2007
>
> Download: [Version 1.2.0 Setup](/uploads/b5402eb4bd4143424ba4ac720584483e/MD5FileChecker1.2Setup.zip) | [Version 1.2.0 Source](/uploads/ce2a4c708829fcaabe07b63b73a0d5cd/MD5FileChecker1.2Source.zip)

**Version 1.1.0:**
> - Bug Fix:  Fixed a bug where files weren't compared correctly 100% of the time.
> - Add an Elapsed Time counter.
> - Released on 15 October 2007
>
> Download: [Version 1.1.0 Setup](/uploads/9d5ef31478e23cc9348052b92dca7d3a/MD5FileChecker1.1Setup.zip) | [Version 1.1.0 Source](/uploads/bb2e89331e3b6a4ff64f8356f3f901ba/MD5FileChecker1.1Source.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 10 October 2007.
>
> Download: [Version 1.0.0 Setup](/uploads/85885e43e82ed8b4d627c08cf4af048a/MD5FileChecker1.0Setup.zip) | [Version 1.0.0 Source](/uploads/0c57b66ce7455545be43590415b3f4dd/MD5FileChecker1.0Source.zip)


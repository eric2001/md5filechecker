using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using Org.Mentalis.Security.Cryptography;
using System.Windows.Forms;

namespace MD5FileChecker
{
	/// <summary>
	/// Summary description for md5.
	/// </summary>
	public class md5
	{
		public md5()
		{
		}

		public string generateMD5 (string fileName)
		{
			// This function generates an MD5 checksum for one file (fileName)
			//	and returns that checksum value as a string.
			//	In the event of an error (ex: no file name) this function will return
			//	an empty string.

			// Make sure fileName has a value.
			if (fileName.Length == 0)
				return "";

			// if the file doesn't exist, exit.
			if (System.IO.File.Exists(fileName) == false)
				return "";

			// Create Variables.
			string hash = "";
			MD5CryptoServiceProvider md5hash = new MD5CryptoServiceProvider();
			byte[] buffer = new byte[4096];

			// Open and read the file, generate the checksum
			FileStream fs = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
			int size = fs.Read(buffer, 0, buffer.Length);
			while(size > 0) 
			{
				md5hash.TransformBlock(buffer, 0, size, buffer, 0);
				size = fs.Read(buffer, 0, buffer.Length);
				Application.DoEvents();
			}
			md5hash.TransformFinalBlock (buffer, 0, 0);
			hash = BytesToHex(md5hash.Hash);
			md5hash.Clear();

			// Close the file, return the checksum as a string.
			fs.Close();
			return hash;
	}

		public static string BytesToHex(byte[] bytes) 
		{
			StringBuilder sb = new StringBuilder(bytes.Length * 2);
			for(int i = 0; i < bytes.Length; i++) 
			{
				sb.Append(bytes[i].ToString("X2"));
			}
			return sb.ToString();
		}
	}
}
